package com.cursospring.batch.stepitemprocesslistener;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.stepitemprocesslistener"})
public class StepItemProcessListenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StepItemProcessListenerApplication.class, args);
    }

}
