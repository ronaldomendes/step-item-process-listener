package com.cursospring.batch.stepitemprocesslistener.mapper;

import com.cursospring.batch.stepitemprocesslistener.dto.EmployeeDTO;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.AGE;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.EMAIL;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.EMPLOYEE_ID;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.FIRSTNAME;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.LASTNAME;

public class EmployeeFileRowMapper implements FieldSetMapper<EmployeeDTO> {

    @Override
    public EmployeeDTO mapFieldSet(FieldSet fieldSet) throws BindException {
        EmployeeDTO dto = new EmployeeDTO();
        dto.setEmployeeId(fieldSet.readString(EMPLOYEE_ID));
        dto.setFirstName(fieldSet.readString(FIRSTNAME));
        dto.setLastName(fieldSet.readString(LASTNAME));
        dto.setEmail(fieldSet.readString(EMAIL));
        dto.setAge(fieldSet.readInt(AGE));
        return dto;
    }
}
