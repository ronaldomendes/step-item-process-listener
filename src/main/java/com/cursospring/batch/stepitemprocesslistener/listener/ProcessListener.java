package com.cursospring.batch.stepitemprocesslistener.listener;

import com.cursospring.batch.stepitemprocesslistener.dto.EmployeeDTO;
import com.cursospring.batch.stepitemprocesslistener.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemProcessListener;

@Slf4j
public class ProcessListener implements ItemProcessListener<EmployeeDTO, Employee> {

    @Override
    public void beforeProcess(EmployeeDTO dto) {
        log.info("Before process: {}", dto.toString());
    }

    @Override
    public void afterProcess(EmployeeDTO dto, Employee employee) {
        log.info("After process: {}", employee.toString());
    }

    @Override
    public void onProcessError(EmployeeDTO dto, Exception e) {
        log.error("On error: {} ", e.getMessage());
    }
}
