package com.cursospring.batch.stepitemprocesslistener.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

@Slf4j
public class JobSkipPolicy implements SkipPolicy {

    @Override
    public boolean shouldSkip(Throwable t, int skipCount) throws SkipLimitExceededException {
        log.error("Skip policy error: {} | Caused by: {}. Skip count: {}", t.getMessage(), t.getCause().getMessage(), skipCount);
        return true;
    }
}
