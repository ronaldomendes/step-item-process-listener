package com.cursospring.batch.stepitemprocesslistener.job;

import com.cursospring.batch.stepitemprocesslistener.dto.EmployeeDTO;
import com.cursospring.batch.stepitemprocesslistener.listener.ProcessListener;
import com.cursospring.batch.stepitemprocesslistener.listener.ReaderListener;
import com.cursospring.batch.stepitemprocesslistener.mapper.EmployeeFileRowMapper;
import com.cursospring.batch.stepitemprocesslistener.model.Employee;
import com.cursospring.batch.stepitemprocesslistener.processor.EmployeeProcessor;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;

import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.AGE;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.CHUNK_SIZE;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.EMAIL;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.EMPLOYEE_ID;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.FIRSTNAME;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.LASTNAME;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.QUALIFIER_NAME;
import static com.cursospring.batch.stepitemprocesslistener.utils.Constants.STEP_NAME;

@Configuration
@AllArgsConstructor
public class DemoJob {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final EmployeeProcessor employeeProcessor;
    private final DataSource dataSource;

    @Qualifier(value = QUALIFIER_NAME)
    @Bean
    public Job demoOneJob() throws Exception {
        return jobBuilderFactory.get(QUALIFIER_NAME)
                .start(stepOneDemo())
                .build();
    }

    private Step stepOneDemo() throws Exception {
        return stepBuilderFactory.get(STEP_NAME)
                .<EmployeeDTO, Employee>chunk(CHUNK_SIZE)
                .reader(employeeReader())
                .processor(employeeProcessor)
                .writer(employeeDBWriterDefault())
                .faultTolerant().skipPolicy(skipPolicy())
                .listener(readerListener())
                .listener(processListener())
                .build();
    }

    @Bean
    @StepScope
    public Resource inputFileResource(@Value("#{jobParameters[fileName]}") final String fileName) throws Exception {
        return new ClassPathResource(fileName);
    }

    @Bean
    @StepScope
    public FlatFileItemReader<EmployeeDTO> employeeReader() throws Exception {
        FlatFileItemReader<EmployeeDTO> reader = new FlatFileItemReader<>();
        reader.setResource(inputFileResource(null));
        reader.setLineMapper(defineMapper());
        return reader;
    }

    private DelimitedLineTokenizer defineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(EMPLOYEE_ID, FIRSTNAME, LASTNAME, EMAIL, AGE);
        lineTokenizer.setDelimiter(DelimitedLineTokenizer.DELIMITER_COMMA);
        return lineTokenizer;
    }

    private DefaultLineMapper<EmployeeDTO> defineMapper() {
        DefaultLineMapper<EmployeeDTO> lineMapper = new DefaultLineMapper<>();
        lineMapper.setLineTokenizer(defineTokenizer());
        lineMapper.setFieldSetMapper(new EmployeeFileRowMapper());
        return lineMapper;
    }

    @Bean
    public JdbcBatchItemWriter<Employee> employeeDBWriterDefault() {
        JdbcBatchItemWriter<Employee> itemWriter = new JdbcBatchItemWriter<>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql("insert into tb_employee (employee_id, first_name, last_name, email, age) values (:employeeId, :firstName, :lastName, :email, :age)");
        itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        return itemWriter;
    }

    @Bean
    public JobSkipPolicy skipPolicy() {
        return new JobSkipPolicy();
    }

    @Bean
    public ReaderListener readerListener() {
        return new ReaderListener();
    }

    @Bean
    public ProcessListener processListener() {
        return new ProcessListener();
    }
}
