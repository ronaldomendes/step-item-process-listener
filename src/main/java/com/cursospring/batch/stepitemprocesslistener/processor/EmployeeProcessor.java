package com.cursospring.batch.stepitemprocesslistener.processor;

import com.cursospring.batch.stepitemprocesslistener.dto.EmployeeDTO;
import com.cursospring.batch.stepitemprocesslistener.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@Slf4j
public class EmployeeProcessor implements ItemProcessor<EmployeeDTO, Employee> {

    @Override
    public Employee process(EmployeeDTO dto) throws Exception {
        Employee employee = new Employee();
        employee.setEmployeeId(dto.getEmployeeId() + new Random().nextInt(1000000));
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmail(dto.getEmail());
        employee.setAge(dto.getAge());
        if (employee.getAge() == 45) {
            throw new ArithmeticException("This age is not allowed");
        }
//        log.info("Inside processor: {}", employee.toString());

        return employee;
    }
}
